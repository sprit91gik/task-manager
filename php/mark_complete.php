<?php
// Database connection details
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "todo_app";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: ". $conn->connect_error);
}

// Get task ID from URL parameter
$taskId = $_GET['id'];

// Update task status to completed
$sql = "UPDATE tasks SET state = 'completed' WHERE id = $taskId";
$result = $conn->query($sql);

if ($result) {
    echo json_encode(['success' => true]);
} else {
    echo json_encode(['success' => false]);
}

$conn->close();
?>