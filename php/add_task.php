<?php
header('Content-Type: application/json');

// Connexion à la base de données
$dsn = 'mysql:host=localhost;dbname=todo_app;charset=utf8';
$username = 'root';
$password = 'root';
$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
];

try {
    $pdo = new PDO($dsn, $username, $password, $options);
} catch (PDOException $e) {
    echo json_encode(['success' => false, 'message' => 'Erreur de connexion : ' . $e->getMessage()]);
    exit();
}

// Récupération des données du formulaire
$title = $_POST['title'] ?? '';
$description = $_POST['description'] ?? '';
$due_date = $_POST['due_date'] ?? null;
$reminder_date = $_POST['reminder_date'] ?? null;
$priority = $_POST['priority'] ?? 0;
$state = $_POST['state'] ?? 'not_started';
$category = $_POST['category'] ?? '';

// Vérification des données requises
if (empty($title)) {
    echo json_encode(['success' => false, 'message' => 'Le titre de la tâche est requis.']);
    exit();
}

// Préparation et exécution de la requête SQL
$sql = "INSERT INTO tasks (title, description, due_date, reminder_date, priority, state, category) 
        VALUES (:title, :description, :due_date, :reminder_date, :priority, :state, :category)";
$stmt = $pdo->prepare($sql);

try {
    $stmt->execute([
        ':title' => $title,
        ':description' => $description,
        ':due_date' => $due_date,
        ':reminder_date' => $reminder_date,
        ':priority' => $priority,
        ':state' => $state,
        ':category' => $category,
    ]);
    
} catch (PDOException $e) {
    echo json_encode(['success' => false, 'message' => 'Erreur lors de l\'ajout de la tâche : ' . $e->getMessage()]);
}

if ($result) {
    // Redirect back to tasks.php with success message
    header('Location: tasks.php?message=Task+added+successfully!');
    exit;
  } else {
    // Redirect back to tasks.php with error message
    header('Location: tasks.php?message=Error+adding+task!');
    exit;
  }
?>
