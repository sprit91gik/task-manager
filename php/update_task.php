<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
header('Content-Type: application/json');

// Connexion à la base de données (réutilisez le code de connexion précédent)
require_once 'database.php';

// Mise à jour du statut de la tâche
$data = json_decode(file_get_contents('php://input'), true);
$id = $data['id'];
$status = $data['status'];

$sql = "UPDATE tasks SET status = :status WHERE id = :id";
$stmt = $pdo->prepare($sql);

if ($stmt->execute([':status' => $status, ':id' => $id])) {
    echo json_encode(['success' => true]);
} else {
    echo json_encode(['success' => false, 'message' => 'Erreur lors de la mise à jour de la tâche.']);
}
?>
