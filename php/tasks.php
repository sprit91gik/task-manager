<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// Connexion à la base de données (réutilisez le code de connexion précédent)
require_once 'database.php';

$sql = "SELECT * FROM tasks ORDER BY due_date ASC";
$stmt = $pdo->query($sql);
$tasks = $stmt->fetchAll();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Liste des Tâches</title>
    <link rel="stylesheet" href="../css/styles.css">
    <script src="../js/main.js" defer></script>
</head>
<body>
    <h1>Liste des Tâches</h1>
    <ul>
        <?php foreach ($tasks as $task): ?>
            <li>
                <div class="task-details">
                    <strong><?php echo htmlspecialchars($task['title']); ?></strong><br>
                    <?php echo nl2br(htmlspecialchars($task['description'])); ?><br>
                    Échéance: <?php echo htmlspecialchars($task['due_date']); ?><br>
                    Rappel: <?php echo htmlspecialchars($task['reminder_date']); ?><br>
                    Priorité: <?php echo htmlspecialchars($task['priority']); ?><br>
                    État: <?php echo htmlspecialchars($task['state']); ?><br>
                    Catégorie: <?php echo htmlspecialchars($task['category']); ?>
                </div>
                <div class="task-actions">
                    <form style="display:inline;">
                        <button type="button" class="mark-complete" data-id="<?php echo $task['id']; ?>">Marquer comme terminée</button>
                    </form>
                    <form style="display:inline;">
                        <button type="button" class="delete-task" data-id="<?php echo $task['id']; ?>">Supprimer</button>
                    </form>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
    <a href="../index.html">Retour</a>
</body>
</html>

