# Application Todo

Une simple application Todo construite avec PHP, JavaScript et Bootstrap.

## Dépendances

- PHP 7.2 ou supérieur
- MySQL 5.6 ou supérieur
- Bootstrap 4.0.0
- jQuery 3.2.1
- JavaScript (syntaxe ES6)

## Installation

1. Clonez le dépôt :
   ```
   git clone https://gitlab.com/your-username/todo-app.git
   ```

2. Créez une nouvelle base de données MySQL et importez le fichier `todo_app.sql`.
3. Mettez à jour le fichier `config.php` avec vos informations d'identification de base de données.
4. Exécutez l'application en accédant à `http://localhost/todo-app` dans votre navigateur Web.

## Configuration
`config.php` : mettez à jour les informations d'identification de la base de données et d'autres paramètres si nécessaire.
`php/mark_complete.php` et `php/delete_task.php` : mettez à jour les détails de connexion à la base de données et les requêtes SQL si nécessaire.

## Usage
- Créer une nouvelle tâche : Remplissez le formulaire et cliquez sur "Ajouter une tâche".
- Marquer une tâche comme terminée : Cliquez sur le bouton « Marquer comme terminée ».
- Supprimer une tâche : Cliquez sur le bouton "Supprimer".
- Afficher toutes les tâches : Cliquez sur le lien "Liste des tâches".

## Licence
Ce projet est sous licence MIT. Voir `LICENCE` pour plus de détails.

## Contribuant
Les contributions sont les bienvenues ! Veuillez créer le référentiel et soumettre une pull request avec vos modifications.

## Problèmes
Si vous rencontrez des problèmes ou avez des questions, veuillez ouvrir un ticket sur le référentiel GitLab.

## Auteur
Jefferson Agbotounso

## Remerciements
- Bootstrap
- jQuery