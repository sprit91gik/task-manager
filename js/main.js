document.addEventListener('DOMContentLoaded', function() {
    // Add event listeners to mark complete and delete task buttons
    const markCompleteButtons = document.querySelectorAll('.mark-complete');
    const deleteTaskButtons = document.querySelectorAll('.delete-task');

    markCompleteButtons.forEach(button => {
        button.addEventListener('click', function(e) {
            e.preventDefault();
            const taskId = this.getAttribute('data-id');
            markTaskComplete(taskId);
        });
    });

    deleteTaskButtons.forEach(button => {
        button.addEventListener('click', function(e) {
            e.preventDefault();
            const taskId = this.getAttribute('data-id');
            deleteTask(taskId);
        });
    });

    // Function to mark task as complete
    function markTaskComplete(taskId) {
        fetch(`../php/mark_complete.php?id=${taskId}`)
           .then(response => response.json())
           .then(data => {
                if (data.success) {
                    // Update task list dynamically
                    const taskElement = document.querySelector(`[data-id="${taskId}"]`);
                    taskElement.classList.add('task-completed');
                    taskElement.querySelector('.mark-complete').disabled = true;
                } else {
                    alert('Erreur : Impossible de marquer la tâche comme terminée.');
                }
            })
           .catch(error => {
                console.error('Erreur lors de la requête fetch :', error);
                alert('Erreur : Une erreur s\'est produite lors de la tentative de marquer la tâche comme terminée.');
            });
    }

    // Function to delete task
    function deleteTask(taskId) {
        if (confirm('Êtes-vous sûr de vouloir supprimer cette tâche?')) {
            fetch(`../php/delete_task.php?id=${taskId}`, {
                method: 'DELETE'
            })
               .then(response => response.json())
               .then(data => {
                    if (data.success) {
                        // Update task list dynamically
                        const taskElement = document.querySelector(`[data-id="${taskId}"]`);
                        taskElement.remove();
                    } else {
                        alert('Erreur : Impossible de supprimer la tâche.');
                    }
                })
               .catch(error => {
                    console.error('Erreur lors de la requête fetch :', error);
                    alert('Erreur : Une erreur s\'est produite lors de la tentative de suppression de la tâche.');
                });
        }
    }
});